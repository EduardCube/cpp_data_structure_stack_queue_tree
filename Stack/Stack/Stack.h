#ifndef STACK_H_
#define STACK_H_
#include <cstdlib> // ���� ��� size_t.
#include <assert.h>
#include <cassert>  

using namespace std;

template <class Type>
class Stack
{
private:
	static const size_t size_ = 10;
	Type* data;        
	int count;
	
	
public:
	bool EMPTY;
	Stack() :count(0), EMPTY(true) { data = new Type[size_]; }
	~Stack() { delete[] data; };
	// ����Բ���ײ ����ֲ�-�����
	void push(const Type& entry);
	void pop();
	size_t size();
	Type top();
	bool empty();
	void clear();
	void unique();
};

template <class Type>
void Stack<Type>::push(const Type& entry)
// ��������������� ���˲�����: cassert
{
	assert(count< size_);
	data[count] = entry;
	++count;
	EMPTY = false;
}

template <class Type>
void Stack<Type>::pop()
// ��������������� ���˲�����: cassert
{
	assert(count > 0);
	data[count - 1] = NULL;
	--count;
	if (count == 0) EMPTY = true;
}

template <class Type>
Type Stack<Type>::top()
{
	assert(count >= 0);
	return data[count - 1];
}
//������� ������� �������
template<typename Type>
void Stack<Type>::clear()
{
	memset(data, NULL, sizeof(data));
}

template<class Type>
void Stack<Type>::unique()
{
	assert(count >= 0);
	for (unsigned i = 0; i < count-1; i++)
	{
		if (data[i] == data[i + 1])
		{
			data[i] = NULL;
			for (unsigned j = i + 1; j < count; j++)
				data[j - 1] = data[j];
			--count;
			i--;
		}
	}
}

template<typename Type>
size_t Stack<Type>::size()
{
	return count;
}
template<typename Type>
bool Stack<Type>::empty()
{
	if (count == 0)
		return true;
	else {
		return false;
	}
}

#endif // STACK_H_