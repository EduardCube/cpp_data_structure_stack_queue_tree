#pragma once
#ifndef QUEUE_H_
#define QUEUE_H_
#include <cstdlib> // ���� ��� size_t.
#include <memory.h>
#include <assert.h>
#include <cassert>  

using namespace std;

template <class Type>
class Queue
{
private:
	static  const size_t SIZE = 12;
	Type* Qu;
	size_t count;
public:
	Queue() :count(0) { Qu = new Type[SIZE]; }
	
	void push(const Type &elem);
	void pop();
	Type front();
	size_t size();
	bool empty() const { return count == 0?true:false; }
	~Queue() { delete[] Qu; }
};


#endif

template<class Type>
 void Queue<Type>::push(const Type &elem)
{
	 assert(count < SIZE);
		 Qu[count] = elem;
	 count++;
}

template<class Type>
 void Queue<Type>::pop()
{
	 assert(empty() == false);
		 Qu[0] = NULL;
	 for (unsigned i(1); i < count; i++)
		 Qu[i - 1] = Qu[i];
	 count--;
}

template<class Type>
 Type Queue<Type>::front()
{
	 assert(empty() == false);
	return Qu[0];
}

template<class Type>
 size_t Queue<Type>::size()
{
	return count;
}
