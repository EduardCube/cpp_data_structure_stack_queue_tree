#pragma once
#include <iostream>

using namespace std;



template <class T>
class Tree
{
private:
	struct node
	{
		T val;
		node *left;
		node *right;
		node() {};
		node(T value) : val(value) {};
		~node() {};
	};
private:
	node *root;
	bool add(const T, node* nod = 0);
	void attach_node(node *, node * &, T);
	void del(const T);
	void find(T, node **, node **);
	bool find_elem(node *, const T);
	void clear(node*);
	void case_a(node *, node *);
	void case_b(node *, node *);
	void case_c(node *, node *);
	void preorder(node *);
	void inorder(node *);
	void postorder(node *);
	void display(node *, int);
	
public:
	Tree();
	bool Add(const T value);
	void Del(const T value);
	bool Find(const T value);
	void Preorder();
	void Inorder();
	void Postorder();
	void Display();
	
	Tree(const T);
	
	~Tree();

};

template<class T>
Tree<T>::Tree()
{
	root = nullptr;
}
template<class T>
 bool Tree<T>::Add(const T value)
{
	 bool check= add(value);
	 if (check) return true;
	 else return false;
}
template<class T>
 void Tree<T>::Del(const T value)
{
	  del(value);
}
template<class T>
 bool Tree<T>::Find(const T value)
{
	 bool nod = find_elem(root,value);
	 if(nod)
	return true;
	 else return false;
}
template<class T>
 void Tree<T>::Preorder()
{
	  preorder(root);
}
template<class T>
 void Tree<T>::Inorder()
{
	  inorder(root);
}
template<class T>
 void Tree<T>::Postorder()
{
	 postorder(root);
}
template<class T>
 void Tree<T>::Display()
{
	 display(root,1);
}
 /*template<class T>
  void Tree<T>::temp()
 {
	 Temp(&root);
 }
 template<class T>
  void Tree<T>::Temp(node **tree)
 {
	  node* lef;
	  node* rig;
	 if (root == nullptr)
	 {
		 cout << "Tree is empty" << endl;
		 return;
	 }
	 if ((*tree)->left != nullptr)
	 {
		 lef = (*tree)->left; 
		 Temp(&lef);
	 }
	 node *parent, *location;
	 find((*tree)->val, &parent, &location);
	 if (location == nullptr)
	 {
		 cout << "Such element does not present at this tree" << endl;
		 return;
	 }
	 if (location->left != nullptr && location->right != nullptr)
	 {
		 case_c(parent, location);
		 (*tree) = parent;
		 delete location;
	 }
	 if ((*tree)->right != nullptr)
	 {
		 rig = (*tree)->right;
		 Temp(&rig);
	 }
 }*/
template<class T>
 Tree<T>::Tree(const T value) 
{
	add(value);
}

template<class T>
 bool Tree<T>::add(const T value, node* nod)
{
	if (!root)
	{
		root = new node(value);
		root->left = nullptr;
		root->right = nullptr;
		return true;
	}
	if (!nod)
	{
		nod = root;
	}
	if ((nod) && nod->val != value)
	{
		if ( value < nod->val)
		{
			if (nod->left)
			{
				add(value, nod->left);
			}
			else {
				attach_node(nod, nod->left, value);
				return true;
			}
		}
		else if (value > nod->val)
		{
			if (nod->right)
			{
				add(value, nod->right);
			}
			else {
				attach_node(nod, nod->right, value);
				return true;
			}
		}
	}
	else if(nod->val==value){
		/*cout << "Such number already is" << endl;*/
		return false;
	}
}

template<class T>
 void Tree<T>::attach_node(node * parent, node *& new_node, T value)
{
	new_node = new node(value);
	new_node->left = nullptr;
	new_node->right = nullptr;
}

template<class T>
 void Tree<T>::del( const T value)
{
	node *parent, *location;
	find(value, &parent, &location);
	if (location == nullptr)
	{
		cout << "Such element does not present at this tree" << endl;
		return;
	}
	if (location->left == nullptr && location->right == nullptr)
		case_a(parent, location);
	if (location->left != nullptr && location->right == nullptr)
		case_b(parent, location);
	if (location->left == nullptr && location->right != nullptr)
		case_b(parent, location);
	if (location->left != nullptr && location->right != nullptr)
		case_c(parent, location);
	delete location;
}

template<class T>
 void Tree<T>::find(T value, node ** parent, node ** location)
{
	 node *ptr, *ptrsave;
	 if (value == root->val)
	 {
		 *parent = nullptr;
		 *location = root;
		 return;
	 }
	 if (value < root->val)
		 ptr = root->left;
	 else ptr = root->right;
	 ptrsave = root;
	 while (ptr)
	 {
		 if (value == ptr->val)
		 {
			 *location = ptr;
			 *parent = ptrsave;
			 return;
		 }
		 ptrsave = ptr;
		 if (value < ptr->val)
			 ptr = ptr->left;
		 else ptr = ptr->right;
	 }
	 *location = nullptr;
	 *parent = ptrsave;
}

template<class T>
 bool Tree<T>::find_elem(node * tree, const T value)
{
	 if (root == nullptr)
	 {
		 /*cout << "Tree is empty" << endl;*/
		 return false;
	 }
	while ((tree) && tree->val != value)
	{
		if (tree->val == value) {
			/*cout << "Element is find";*/
			return true;
		}
		if (value < tree->val)
			tree = tree->left;
		else 
			tree = tree->right;
	}
	if (!tree)
		return false;
		/*cout << "Element is not find" << endl;*/
}

 template<class T>
  void Tree<T>::clear(node *tree)
 {
	  if (!tree)
		  return;
	  
		  if (tree->left)
		  {
			  clear(tree->left);
			  tree->left = nullptr;
		  }
		  if (tree->right)
		  {
			  clear(tree->right);
			  tree->right = nullptr;
		  }
	  
		  delete tree;
 }

 template<class T>
  void Tree<T>::case_a(node *par, node *loc)
 {
	 if (par == nullptr)
		 root = nullptr;
	 else 
	 {
		 if (loc == par->left)
			 par->left = nullptr;
		 else
			 par->right = nullptr;
	 }
 }

 template<class T>
  void Tree<T>::case_b(node *par, node *loc)
 {
	  node *child;
	  if (loc->left != nullptr)
		  child = loc->left;
	  else 
		  child = loc->right;
	  if (par == nullptr)
	  {
		  root = child;
	  }
	  else
	  {
		  if (loc == par->left)
			  par->left = child;
		  else
			  par->right = child;
	  }
 }

 template<class T>
  void Tree<T>::case_c(node *par, node *loc)
 {
	  node *suc, *ptr, *ptrsave, *ptrsuc;
	  ptrsave = loc;
	  ptr = loc->right;
	  while (ptr->left != nullptr)
	  {
		  ptrsave = ptr;
		  ptr = ptr->left;
	  }
	  suc = ptr;
	  ptrsuc = ptrsave;
	  if (suc->left == nullptr && suc->right == nullptr)
	  {
		  case_a(ptrsuc, suc);
	  }
	  else
		  case_b(ptrsuc, suc);
	  if (par == nullptr)
		  root = suc;
	  else if (loc == par->left)
		  par->left = suc;
	  else
		  par->right = suc;
	  suc->left = loc->left;
	  suc->right = loc->right;
 }

  template<class T>
   void Tree<T>::preorder(node *tree)
  {
	  if (root == nullptr)
	  {
		  cout << "Tree is empty" << endl;
		  return;
	  }
	  cout << tree->val << "  ";

	  if (tree->left)
		  preorder(tree->left);
	  if (tree->right)
		  preorder(tree->right);
	  
  }

  template<class T>
   void Tree<T>::inorder(node *tree)
  {
	  if (root == nullptr)
	  {
		  cout << "Tree is empty" << endl;
		  return;
	  }
	  if (tree->left != nullptr)
		  inorder(tree->left);
	  cout << tree->val << "  ";
	  if (tree->right != nullptr)
		  inorder(tree->right);
  }

  template<class T>
   void Tree<T>::postorder(node *tree)
  {
	  if (root == nullptr)
	  {
		  cout << "Tree is empty" << endl;
		  return;
	  }
	      if(tree->left != nullptr)
			  postorder(tree->left);
		  if (tree->right != nullptr)
			  postorder(tree->right);
		  cout << tree->val << "  ";
	  
  }

  template<class T>
   void Tree<T>::display(node *tree, int level)
  {
	  int i;
	  if (tree != nullptr)
		  display(tree->right, level + 1);
		  cout << endl;
		  if (tree == root)
			  cout << "Root->:  ";
		  else
		  {
			  for (i = 0; i < level; i++)
				  cout << "    ";
		  }
		  if(tree!=nullptr)
		  cout << tree->val;
		  else 
		  cout << "NULL";
		  if (tree != nullptr)
		  display(tree->left, level + 1);
	  
  }

  template<class T>
   Tree<T>::~Tree()
  {
	  clear(root);
  }
