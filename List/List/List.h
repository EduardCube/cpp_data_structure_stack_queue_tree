#pragma once
#include <string>
#include <iostream>
#include <conio.h>
#include <vector>
#include <clocale>
using namespace std;


template <class T>
class LIST
{
private:
		struct List
		{
			T ex;
			List* next;
		};
		List *root;
private:
	void print(List *beg);
	T get_element(List *beg);
	void init(List **beg);
	void push_front(List **begin, const T e);
	void insert(List **begin, const T e);
	void push_back(List **begin, const T e);
	void Delete(List **begin, const T e);
	void L4(List **begin);
	void pop_front(List **begin);
	List * find(List *u, T x);
	void clear(List **begin);
public:
	LIST();
	LIST(T value);
	void Print();
	T getElement();
	void Init();
	void Push_front( const T e);
	void Insert( const T e);
	void Push_back( const T e);
	void Remove( const T e);
	void l4();
	void Pop_front();
	List * Find(T x);
	~LIST();
};

template<class T>
 LIST<T>::LIST()
{
	root = NULL;
}

template<class T>
 LIST<T>::LIST(T value)
{
	root = NULL;
	push_back(root, value);
}

 template<class T>
  void LIST<T>::Print()
 {
	 print(root);
 }

template<class T>
 T LIST<T>::getElement()
{
	return get_element(root);
}

template<class T>
 void LIST<T>::Init()
{
	 init(&root);
}

template<class T>
 void LIST<T>::Push_front(const T e)
{
	 push_front(&root, e);
}

template<class T>
 void LIST<T>::Insert(const T e)
{
	 insert(&root, e);
}

template<class T>
 void LIST<T>::Push_back(const T e)
{
	 push_back(&root, e);
}

template<class T>
 void LIST<T>::Remove(const T e)
{
	 Delete(&root, e);
}

template<class T>
 void LIST<T>::l4()
{
	 L4(&root);
}

template<class T>
 void LIST<T>::Pop_front()
{
	 pop_front(&root);
}

template<class T>
typename LIST<T>::List*
 LIST<T>::Find(T x)
{
	return find(root, x);
}

 template<class T>
  LIST<T>::~LIST()
 {
	 clear(&root);
 }

template<class T>
 void LIST<T>::print(List * beg)
{
	 List *print = beg;
	 while (print)
	 {
		 cout << print->ex << " -> ";
		 print = print->next;
	 }
	 cout << "NULL" << endl;
}

template<class T>
 T LIST<T>::get_element(List * beg)
{
	 List *get = beg;
	 return get->ex;
}

template<class T>
 void LIST<T>::init(List ** beg)
{
	 *beg = new List;
	 (*beg)->ex = 0;
	 (*beg)->next = NULL;
}

template<class T>
 void LIST<T>::push_front(List ** begin, const T e)
{
	 List* temp = new List;
	 temp->ex = e;
	 temp->next = *begin;
	 *begin = temp;
}

template<class T>
 void LIST<T>::insert(List ** begin, const T e)
{
	 List* ins = new List;
	 ins->ex = e;
	 if (*begin == NULL)
	 {
		 ins->next = NULL;
		 *begin = ins;
		 return;
	 }
	 List* be = *begin;
	 if (be->ex >= ins->ex)
	 {
		 ins->next = be;
		 *begin = ins;
		 return;
	 }
	 List* en = be->next;
	 while (en)
	 {
		 if (be->ex < ins->ex&& ins->ex <= en->ex)
		 {
			 be->next = ins;
			 ins->next = en;
			 return;
		 }
		 be = en;
		 en = en->next;
	 }
	 be->next = ins;
	 ins->next = NULL;
}

template<class T>
 void LIST<T>::push_back(List ** begin, const T e)
{
	 if (*begin == NULL)
	 {
		 *begin = new List;
		 (*begin)->ex = e;
		 (*begin)->next = NULL;
		 return;
	 }
	 List* end = new List;
	 end->ex = e;
	 end->next = NULL;
	 List* be = *begin;

	 while (true)
	 {
		 if (be->next == NULL)
		 {
			 be->next = end;
			 break;
		 }
		 be = be->next;

	 }
}

template<class T>
 void LIST<T>::Delete(List ** begin, const T e)
{
	 if (*begin == NULL)
	 {
		 return;
	 }
	 List *be = *begin;
	 if (be->ex == e)
	 {
		 *begin = be->next;
		 delete be;
		 return;
	 }
	 List* en = be->next;
	 while (en)
	 {
		 if (en->ex == e)
		 {
			 be->next = en->next;
			 delete en;
			 return;
		 }
		 be = en;
		 en = en->next;
	 }
}

template<class T>
 void LIST<T>::L4(List ** begin)
{
	 bool check = true;
	 if (*begin == NULL)
	 {
		 return;
	 }
	 List *be = *begin;
	 List* en = be->next;

	 while (en)
	 {
		 List* N = en;
		 if (check == true)
		 {
			 be->next = en->next;
			 delete N;
			 en = be->next;
			 check = false;
		 }
		 else
		 {
			 check = true;
			 be = en;
			 en = en->next;
			 N = en;
		 }
	 }
}

template<class T>
 void LIST<T>::pop_front(List ** begin)
{
	 if (*begin == 0)
		 return;
	 List *be = *begin;
	 *begin = be->next;
	 delete be;
}

template<class T>
typename LIST<T>::List*
 LIST<T>::find(List * u, T x)
{
	 List *p = u;
	 while (p)
	 {
		 if (p->ex == x) // ����� ��� ������
			 return p;
		 p = p->next;
	 }
	 return 0;
}

template<class T>
 void LIST<T>::clear(List ** begin)
{
	 if (*begin == 0)
		 return;

	 List *b = *begin;
	 List *t;
	 while (b)
	 {
		 t = b;
		 b = b->next;
		 delete t;
	 }
	 *begin = NULL;
}
